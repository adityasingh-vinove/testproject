import { Router } from 'express';
import AuthController from '../controllers/AuthController';
const authJwt = require("../src/middlewares/authJwt");
const router = Router();

router.post('/signup', AuthController.signUp);
router.post('/login',AuthController.signIn);
router.post('/forgot-password',AuthController.forgot);
router.put('/reset-password',authJwt.verifyToken,AuthController.reset);
router.get('/logout',authJwt.verifyToken,AuthController.logout);
export default router;