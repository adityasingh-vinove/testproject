import AuthService from '../services/AuthService';
import Util from '../utils/Utils';
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

const util = new Util();

class AuthController {

    static async signUp(req, res) {
      if (!req.body.name || !req.body.phone || !req.body.username || !req.body.email || !req.body.age || !req.body.gender
        || !req.body.location || !req.body.password) {
        util.setError(400, 'Please provide complete details');
        return util.send(res);
      }
      try {
          let obj={
            name : req.body.name,
            phone: req.body.phone,
            username: req.body.username,
            email: req.body.email,
            age: req.body.age,
            gender: req.body.gender,
            location: req.body.location,
            password: bcrypt.hashSync(req.body.password, 8)}
        const user = await AuthService.addUser(obj);
        util.setSuccess(201, 'User registered successfully!', user);
        return util.send(res);
      } catch (error) {
        util.setError(400, error.message);
        return util.send(res);
      }   
    } 


    static async signIn(req, res) {
        if (!req.body.username || !req.body.password) {
          util.setError(400, 'Please provide complete details');
          return util.send(res);
        }
        try {
          const user = await AuthService.findOne({username:req.body.username});
          if (!user) {
            return res.status(404).send({ message: "User Not found." });
          }
          else{
            var passwordIsValid = bcrypt.compareSync(
                req.body.password,
                user.password
              );
        
              if (!passwordIsValid) {
                return res.status(401).send({
                  accessToken: null,
                  message: "Invalid Password!"
                });
              }
              else{
                var token = jwt.sign({ id: user.id }, process.env.SECRET_KEY, {
                    expiresIn: 86400 // 24 hours
                  });
                  util.setSuccess(201, 'LogedIn successfully!', {
                    id: user.id,
                    name: user.name,
                    phone: user.phone,
                    username: user.username,
                    email: user.email,
                    accessToken: token
                  });
                return util.send(res);
              } 
          }
        } catch (error) {
          util.setError(400, error.message);
          return util.send(res);
        }   
      } 


      static async forgot(req, res) {
        if (!req.body.email) {
          util.setError(400, 'Please provide complete details');
          return util.send(res);
        }
        try {
            const user = await AuthService.findOne({email:req.body.email});
            if (!user) {
            return res.status(404).send({ message: "User Not found." });
          }
          else{
                var token = jwt.sign({ id: user.id }, process.env.SECRET_KEY, {
                    expiresIn: 86400 // 24 hours
                  });
                  util.setSuccess(201, 'Mail sent successfully!', {
                    email: user.email,
                    accessToken: token
                  });
                return util.send(res);

          }
        } catch (error) {
          util.setError(400, error.message);
          return util.send(res);
        }   
      } 

      static async reset(req, res) {
        if (!req.body.password) {
          util.setError(400, 'Please provide complete details');
          return util.send(res);
        }
        try {
            let condition={id:req.userId};
            let updateObj={password:bcrypt.hashSync(req.body.password, 8)};

            const updateUser = await AuthService.update(condition,updateObj);
            if (!updateUser) {
                return res.status(404).send({ message: "User Not found." });
              }
              else{
                util.setSuccess(201, 'Password reset successfully!');
                return util.send(res);
              }

        } catch (error) {
          util.setError(400, error.message);
          return util.send(res);
        }   
      } 

      static async logout(req, res) {
        try {
            const invalidToken = await AuthService.addInvalidToken({
                token: req.headers["x-access-token"]});
            if (!invalidToken) {
                return res.status(404).send({ message: "User Not found." });
              }
              else{
                util.setSuccess(201,'Loged out successfully!');
                return util.send(res);
              }

        } catch (error) {
          util.setError(400, error.message);
          return util.send(res);
        }   
      } 
  
  }
  
  export default AuthController;