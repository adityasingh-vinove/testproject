require('dotenv').config();


module.exports = {

  development: {
    database: 'test_db',
    username: 'test_user',
    password: 'test_2020',
    host: 'localhost',
    dialect: 'postgres'
  },

  test: {
    database: 'forte_test',
    username: 'steven',
    password: null,
    host: '127.0.0.1',
    dialect: 'postgres'
  },

  production: {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: 'postgres'
  }
};