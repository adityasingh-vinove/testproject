module.exports = (sequelize, DataTypes) => {
  const Videos = sequelize.define('Videos', {
    userId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    videoName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    s3Location: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    isDeleted: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  });
  return Videos;
};
