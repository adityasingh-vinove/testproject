module.exports = (sequelize, DataTypes) => {
  const EntityManagement = sequelize.define('EntityManagement', {
    userId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    videoId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    categoryId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    action: {
      type: DataTypes.ENUM('like','comment','rate'),
      allowNull: false,
    },
    content: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    count: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
  return EntityManagement;
};
