const jwt = require("jsonwebtoken");
import database from '../models';

const verifyToken = async(req, res, next) => {
  let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }

  try {
    const invalidToken = await database.InvalidTokens.findOne({
      where: {token:token}
    });
    if(invalidToken)
      {
        return res.status(401).send({ message: "Unauthorized!" });
      }
      else{
        jwt.verify(token,  process.env.SECRET_KEY, (err, decoded) => {
          if (err) {
            return res.status(401).send({ message: "Unauthorized!" });
          }
          req.userId = decoded.id;
          next();
        });
      }

  } catch (error) {
    return res.status(401).send({ message: "Unauthorized!" });
}

};

const authJwt = {
    verifyToken: verifyToken,
  };
  module.exports = authJwt;