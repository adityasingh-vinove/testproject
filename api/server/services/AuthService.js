import database from '../src/models';


class AuthService {

    static async addUser(newUser) {
      try {
        return await database.Users.create(newUser);
      } catch (error) {
        throw error;
      }
    }

    static async findOne(condition) {
        try {
          const user = await database.Users.findOne({
            where: condition
          });
   
          return user;
        } catch (error) {
          throw error;
        }
      }

      static async update(condition,updateObj) {
        try {
          return  await database.Users.update(updateObj, { where: condition });   
        } catch (error) {
          throw error;
        }
      }

      static async addInvalidToken(obj) {
        try {
          return  await database.InvalidTokens.create(obj);   
        } catch (error) {
          throw error;
        }
      }
  
  }

  export default AuthService;