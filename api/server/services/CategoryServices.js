import database from '../src/models';

class CategoryService {

  static async addCategory(newCategory) {
    try {
      return await database.Categories.create(newCategory);
    } catch (error) {
      throw error;
    }
  }

  static async updateCategory(id, updateCategory) {
    try {
      const categoryToUpdate = await database.Categories.findOne({
        where: { id: Number(id) }
      });

      if (categoryToUpdate) {
        await database.Categories.update(updateCategory, { where: { id: Number(id) } });

        return updateCategory;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getACategory(id) {
    try {
      const theCategory = await database.Categories.findOne({
        where: { id: Number(id) }
      });

      return theCategory;
    } catch (error) {
      throw error;
    }
  }


}

export default CategoryService;
