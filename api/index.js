
import config from 'dotenv';
import express from 'express';
import bodyParser from 'body-parser';
var fileupload = require("express-fileupload");
var CronJob = require('cron').CronJob;
import CategoryRoutes from './server/routes/CategoryRoutes';
import AuthRoutes from './server/routes/AuthRoutes';
import VideoRoutes from './server/routes/VideoRoutes';
import UserRoutes from './server/routes/UserRoutes';
import ReportReasonRoutes from './server/routes/ReportReasonRoutes';
import ChallengeRoutes from './server/routes/ChallengeRoute';
import TaskRoutes from './server/routes/TaskRoutes';
import CroneJob from './server/utils/crone/CroneJob';

config.config();

const app = express();

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({
//   parameterLimit: 100000,
//   limit: '30mb',
//   extended: true
// }));
// app.use(bodyParser.OptionsUrlencoded({limit: '500mb',extended: true}));
// app.use(bodyParser.OptionsJson({limit: '500mb'}));
app.use(bodyParser({limit: '1500mb'}));
app.use(fileupload());



const port = process.env.PORT || 8000;

app.use('/api/v1/category', CategoryRoutes);
app.use('/api/v1/auth',AuthRoutes);
try{
  app.use('/api/v1/video',VideoRoutes);
  console.log("success--------------->1")
}catch(error){
  console.log("error----------------------------1",error)}
app.use('/api/v1/user',UserRoutes);
app.use('/api/v1/report',ReportReasonRoutes);
app.use('/api/v1/challenge',ChallengeRoutes);
app.use('/api/v1/task',TaskRoutes);

// when a random route is inputed
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to this API.',
}));
// var job1 = new CronJob('00 00 00 * * 0', function() {
//   CroneJob.declareChallengeWinner();
// }, null, true, 'America/Los_Angeles');
// job1.start();

// var job2 = new CronJob('00 00 00 * * *', function() {
//   CroneJob.deleteAccount();
// }, null, true, 'America/Los_Angeles');
// job2.start();
// app.listen(port, () => {
//   console.log(`Server is running on PORT ${port}`);
// });

export default app;
